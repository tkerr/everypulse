/******************************************************************************
 * EveryPulseExample.ino
 * Copyright (c) 2021 Tom Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * EveryPulse library example sketch.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "EveryPulse.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define BOARD_LED_PIN   (13)      //!< The Arduino LED pin
#define SERIAL_BAUD     (115200)  //!< Serial port baud rate
#define SERIAL_BUF_SIZE (16)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/


/******************************************************************************
 * Local objects and data.
 ******************************************************************************/
static char serialBuf[SERIAL_BUF_SIZE];
static int  serialBufIdx = 0;


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * setup
 **************************************/
void setup()
{
    // Setup the EveryPulse object to blink the LED.
    EveryPulse.begin(BOARD_LED_PIN);
    EveryPulse.setFrequency(2);      // Initial rate = 2 Hz
    
    // Setup the serial port.
    Serial.begin(SERIAL_BAUD);
    delay(1000);
    Serial.println("EveryPulse example sketch");
    Serial.println("Enter the LED blink rate in Hz");
}


/**************************************
 * loop
 **************************************/
void loop()
{
    unsigned int hz;
    
    // Check the serial port for an incoming blink rate.
    if (Serial.available())
    {
        // Buffer overflow.
        if (serialBufIdx >= SERIAL_BUF_SIZE) serialBufIdx = 0;
        
        // Read the next character.
        char c = Serial.read();
        
        // Check for end of line.
        if ((c == '\r') || (c == '\n'))
        {
            // Found end of line, convert to integer.
            if (serialBufIdx > 0)
            {
                hz = atoi(serialBuf);
                Serial.print("Blink rate: "); 
                Serial.print(hz);
                Serial.println(" Hz"); 
            
                // Set the new frequency.
                EveryPulse.setFrequency(hz);
                serialBufIdx = 0;
            }
        }
        else
        {
            // Add character to serial buffer.
            serialBuf[serialBufIdx++] = c;
            serialBuf[serialBufIdx] = 0;
        }
    }
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/


// End of file.
