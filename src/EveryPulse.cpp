/******************************************************************************
 * EveryPulse.cpp
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Square wave pulse generator for Arduino Nano Every.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "EveryPulse.h"
#include "EveryTimerB.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
static void everyPulseIsr();


/******************************************************************************
 * Local definitions.
 ******************************************************************************/

/****
 * Uncomment exactly one of the following to determine which Timer B to use.
 ****/
//#define PULSE_USE_TIMER_B0
#define PULSE_USE_TIMER_B1
//#define PULSE_USE_TIMER_B2
//#define PULSE_USE_TIMER_B3


/******************************************************************************
 * Global data and objects.
 ******************************************************************************/

/**
 * @brief A single global EveryPulseClass object for use by the application.
 */
EveryPulseClass EveryPulse;


/******************************************************************************
 * Local data and objects.
 ******************************************************************************/
static EveryTimerB PulseTimer;

static TCB_t* _timer =
#if defined(PULSE_USE_TIMER_B0)
    &TCB0;
#elif defined(PULSE_USE_TIMER_B1)
    &TCB1;
#elif defined(PULSE_USE_TIMER_B2)
    &TCB2;
#elif defined(PULSE_USE_TIMER_B3)
    &TCB3;
#else
    #error "Timer B not specified"
#endif


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/

/**************************************
 * EveryPulseClass::EveryPulseClass
 **************************************/
EveryPulseClass::EveryPulseClass() :
    m_freq_hz(0),
    m_period_us(0),
    m_pin(-1)
{
    // Nothing else to do.
}


/**************************************
 * EveryPulseClass::begin
 **************************************/
void EveryPulseClass::begin(int pin)
{
    if (pin >= 0)
    {
        m_pin = pin;
        pinMode(m_pin, OUTPUT);
        digitalWrite(m_pin, 0);
        PulseTimer.initialize(_timer);
        PulseTimer.attachInterrupt(everyPulseIsr);
        PulseTimer.stop();
    }
}


/**************************************
 * EveryPulseClass::end
 **************************************/
void EveryPulseClass::end()
{
    PulseTimer.stop();
    PulseTimer.detachInterrupt();
}


/**************************************
 * EveryPulseClass::setFrequency
 **************************************/
void EveryPulseClass::setFrequency(uint32_t freq_hz)
{
    if (freq_hz == 0)
    {
        stop();
    }
    else if (freq_hz <= PULSE_MAX_FREQ_HZ)
    {
        m_freq_hz = freq_hz;
        m_period_us = 500000UL / freq_hz;
        PulseTimer.setPeriod(m_period_us);
    }
}


/**************************************
 * EveryPulseClass::start
 **************************************/
void EveryPulseClass::start()
{
    stop();
    PulseTimer.setPeriod(m_period_us);
}


/**************************************
 * EveryPulseClass::stop
 **************************************/
void EveryPulseClass::stop()
{
    PulseTimer.stop();
    if (m_pin >= 0)
    {
        digitalWrite(m_pin, 0);
    }
}


/**************************************
 * EveryPulseClass::timerIsr
 **************************************/
void EveryPulseClass::timerIsr()
{
    static int state = 0;
    
    if (m_pin >= 0)
    {
        state = 1 - state;
        digitalWrite(m_pin, state);
    }
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/
 
/**************************************
 * everyPulseIsr
 **************************************/ 
static void everyPulseIsr()
{
    EveryPulse.timerIsr();
}


/**************************************
 * Timer B Interrupt Service Routine
 **************************************/
#if defined(PULSE_USE_TIMER_B0)
ISR(TCB0_INT_vect)
#elif defined(PULSE_USE_TIMER_B1)
ISR(TCB1_INT_vect)
#elif defined(PULSE_USE_TIMER_B2)
ISR(TCB2_INT_vect)
#elif defined(PULSE_USE_TIMER_B3)
ISR(TCB3_INT_vect)
#endif
{
    PulseTimer.next_tick();
    _timer->INTFLAGS = TCB_CAPT_bm;
}


// End of file.
