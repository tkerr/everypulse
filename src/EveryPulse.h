/*****************************************************************************
 * EveryPulse.h
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * Square wave pulse generator for Arduino Nano Every.
 */
#ifndef _EVERY_PULSE_H_
#define _EVERY_PULSE_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
#define PULSE_MAX_FREQ_HZ (100000)  //!< Maximum pulse frequency

 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @brief
 * Square wave pulse generator class for Arduino Nano Every.
 *
 * Uses a Mega AVR Timer B to generate a 50% duty cycle square wave on a 
 * specified output pin.
 *
 * This class uses the EveryTimerB library by Kees van der Oord.
 * https://github.com/Kees-van-der-Oord/Arduino-Nano-Every-Timer-Controller-B
 */
class EveryPulseClass
{
public:

    EveryPulseClass();    //!< Default constructor
    
    void begin(int pin);  //!< Initialize the pulse generator object
    void end();           //!< Disable the pulse generator object
    
    void setFrequency(uint32_t freq_hz);  //!< Set pulse frequency in Hz and start
    inline uint32_t getFrequency() {return m_freq_hz;}
    
    void start();         //!< Start pulses on the output pin
    void stop();          //!< Stop pulses on the output pin
    
    void timerIsr();      //!< Timer ISR - Do not call this directly!
    
private:

    uint32_t m_freq_hz;
    uint32_t m_period_us;
    int      m_pin;
};

// Declare single global EveryPulseClass object for use by the application.
extern EveryPulseClass EveryPulse;

#endif // _EVERY_PULSE_H_
