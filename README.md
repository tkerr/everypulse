# EveryPulse #
Square wave pulse generator class for Arduino Nano Every.

Uses a Mega AVR Timer B to generate a 50% duty cycle square wave on a 
specified output pin.

This class uses the EveryTimerB library by Kees van der Oord.
https://github.com/Kees-van-der-Oord/Arduino-Nano-Every-Timer-Controller-B

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
